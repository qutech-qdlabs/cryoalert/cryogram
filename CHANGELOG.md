# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.1] - 2024-06-20

### Added

### Fixed

### Changed

### Removed

- Removed some dangling debug print statements

## [2.0.0] - 2024-05-24

### Added

- Added Changelog
- Added MIT License
- Added logging to log file

### Fixed

### Changed

- Make use of CryoAdmin API to determine if user is allowed access to CryoGram Functionality
- Make use of CryoAdmin API to retrieve the list of known fridges the requesting user is allowed to access.
- CryoGram is now also published as a docker container, as well as a pypi package.

### Removed
