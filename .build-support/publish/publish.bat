@echo off 
REM To be run in root folder as .build-support/publish/publish.bat

REM =======================
REM Obtain package version
REM =======================
REM run commands for getting the versions of the package
REM generate ESC character for some reason
for /F %%a in ('echo prompt $E ^| cmd') do @set "ESC=%%a"
for /f %%a in ('poetry run python -c "import cryo_gram; print(cryo_gram.__version__)"') do set "package_version=%%a"

REM =======================
REM Publish package
REM =======================
echo Publish package
poetry publish --build -r gitlab-tudelft

REM =======================
REM Build and publish containers
REM =======================
echo Build and publish containers
docker compose build cryogram
docker tag registry.tudelft.nl/cryo_alert/cryo_gram:latest registry.tudelft.nl/cryo_alert/cryo_gram:%package_version%
docker push registry.tudelft.nl/cryo_alert/cryo_gram:latest
docker push registry.tudelft.nl/cryo_alert/cryo_gram:%package_version%