import os

import requests


class CryoAdminClient:
    def __init__(self) -> None:
        self._client = requests.Session()
        self._base_url = os.environ.get("CRYO_ADMIN_BASE_URL")
        if not self._base_url.endswith("/"):
            self._base_url = f"{self._base_url}/"

    def __del__(self):
        self._client.close()

    def get_headers(self):
        return {"Authorization": f"Api-Key {os.environ.get('CRYO_ADMIN_API_KEY')}"}

    def is_allowed_user(self, sending_username: str) -> bool:
        params = {"sending_username": sending_username}

        resp = self._client.get(f"{self._base_url}api/v1/allowed_user/", params=params, headers=self.get_headers())
        # If status_code=200, then the user is allowed. In all other cases, it is not
        return resp.status_code == 200

    def get_known_fridges(self, sending_username: str) -> list[str]:
        params = {"sending_username": sending_username}

        resp = self._client.get(f"{self._base_url}api/v1/known_fridges/", params=params, headers=self.get_headers())
        # If status_code=200, then the user is allowed. In all other cases, it is not
        if resp.status_code == 200:
            return resp.json()
        else:
            return []

    def get_chat_id(
        self, sending_username: str, fridge_name: str, org_name: str | None
    ) -> tuple[int | None, str | None]:
        params = {"sending_username": sending_username, "fridge_name": fridge_name}
        if org_name:
            params.update({"org_name": org_name})

        resp = self._client.get(f"{self._base_url}api/v1/chat_id/", params=params, headers=self.get_headers())
        if resp.status_code == 200:
            return resp.json().get("chat_id"), None
        elif resp.status_code == 400:
            return None, resp.json().get("error_message")
        else:
            return None, f"Unknown error with status_code={resp.status_code} was returned"
