import os
from pathlib import Path

import click
from dotenv import load_dotenv
from pyfiglet import Figlet

from cryo_gram.utils.logging import init_logging


def _print_logo():
    f = Figlet(font="slant")
    print("=" * 30)
    print(f.renderText("CryoGram"))
    print("=" * 30)


def _setup():
    # Setup log folder if not exists
    logs_path = Path.cwd() / "logs"
    if not logs_path.exists():
        logs_path.mkdir()


@click.group()
def cli():
    """
    CLI commands to run CryoGram
    """


@cli.command()
def start():
    """
    CLI command to start CryoGram
    """

    # Load from .env if exist
    load_dotenv()

    # Only load now, because it constructs a GLOBAL CryoAdminClient which needs info from the environment
    # This is a hack, but a working one
    from cryo_gram.bot import CryoGram

    # Init logging
    init_logging(os.environ.get("LOG_LEVEL", "INFO"))

    # print logo
    _print_logo()

    # Setup as needed
    _setup()

    # Create CryoGram
    cryo_gram = CryoGram()

    # RUN with long polling
    cryo_gram.run_polling()


if __name__ == "__main__":
    start()
