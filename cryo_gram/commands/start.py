from telegram import Update
from telegram.ext import ContextTypes

from cryo_gram.commands.utils import is_allowed_user


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    # Construct basic message
    msg = f"Hi {update.effective_user.first_name}, I'm CryoAlertBot.\n"

    # If user is allowed or not, update message
    if is_allowed_user(update, context):
        msg += "You are in my list of allowed users! If you need anything from me, please let me know!"
    else:
        msg += "I'm sorry, but you are not in my list of allowed users. If this is a mistake, please contact the administrator to add you to my list of allowed users."

    # Send message
    await context.bot.send_message(chat_id=update.effective_chat.id, text=msg)
