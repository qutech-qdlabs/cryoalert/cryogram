from telegram import Update
from telegram.ext import ContextTypes

from cryo_gram.commands.utils import get_known_fridges, require_allowed_user


@require_allowed_user
async def get_fridge_list(update: Update, context: ContextTypes.DEFAULT_TYPE):
    # Load all known fridges
    known_fridges = get_known_fridges(sending_username=update.effective_user.username)
    if len(known_fridges) == 0:
        # Send the list of known fridge names
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"I'm sorry, There are no known fridges available for you",
        )
    else:
        # Send the list of known fridge names
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=f"The known fridges are: {known_fridges}",
        )
