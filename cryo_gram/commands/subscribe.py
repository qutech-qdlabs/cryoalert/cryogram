import logging
import re
from datetime import datetime, timedelta
from typing import Optional

from telegram import Update, constants
from telegram.ext import ContextTypes

from cryo_gram.commands.utils import get_chat_id, require_allowed_user

LOGGER = logging.getLogger(__name__)
REGEX_SUBSCRIBE = re.compile(
    "^/subscribe(@CryoAlertBot)? (?P<fridge_name>[a-zA-Z_0-9\-]+)(@(?P<org_name>[a-zA-Z_0-9\-]+))?$"
)


async def _send_message(update: Update, context: ContextTypes.DEFAULT_TYPE, msg: str):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=msg,
    )


async def _check_valid_chat_type(update: Update, context: ContextTypes.DEFAULT_TYPE) -> bool:
    if update.effective_chat.type != constants.ChatType.PRIVATE:
        msg = "If you want to subscribe to a group, please talk to me personally"
        await _send_message(update, context, msg)
        return False

    return True


async def _check_valid_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> tuple[Optional[re.Match], bool]:
    m = REGEX_SUBSCRIBE.match(update.message.text)
    if m is None:
        LOGGER.debug("No valid command was found!")
        msg = "I'm sorry, but no valid command was found"
        await _send_message(update, context, msg)
        return None, False

    return m, True


async def _check_valid_fridge_name(update: Update, context: ContextTypes.DEFAULT_TYPE, fridge_name: str) -> bool:
    if not fridge_name:
        LOGGER.debug("No valid fridge was found!")
        msg = "I'm sorry, but no valid fridge was found"
        await _send_message(update, context, msg)
        return False

    return True


async def _get_chat_id(
    update: Update, context: ContextTypes.DEFAULT_TYPE, fridge_name: str, org_name: str | None
) -> tuple[Optional[int], bool]:
    chat_id, error = get_chat_id(
        sending_username=update.effective_user.username, fridge_name=fridge_name, org_name=org_name
    )
    if error:
        await _send_message(update, context, msg=error)
        return None, False

    return chat_id, True


@require_allowed_user
async def subscribe(update: Update, context: ContextTypes.DEFAULT_TYPE):
    # First, check if this command is run in a private chat or a group
    if not await _check_valid_chat_type(update, context):
        return

    ## Get fridge name from command command
    m, success = await _check_valid_command(update, context)
    if not success:
        return

    # Check if chosen_fridge_name is valid
    chosen_fridge_name = m.group("fridge_name")
    if not await _check_valid_fridge_name(update, context, chosen_fridge_name):
        return

    # Get chat id from chat, potentially in different organization than the user if they are admin
    chosen_org_name = m.group("org_name")
    chat_id, success = await _get_chat_id(update, context, chosen_fridge_name, chosen_org_name)
    if not success:
        return

    # Get chat
    chat = await context.bot.getChat(chat_id)

    # create invite link if it does not exist
    if chat.invite_link:
        LOGGER.debug(f"Found invite link, returning: {chat.invite_link}")
        invite_link = chat.invite_link
    else:
        LOGGER.debug(f"Found no invite link, creating new one")
        invite_link = await chat.create_invite_link(expire_date=datetime.now() + timedelta(hours=1))
        LOGGER.debug(f"Created new invite link, returning: {invite_link}")

    # Return a message with the invite link
    LOGGER.debug(f"Sending invite link: {invite_link}")
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"User Allowed: Check\nValid Fridge Selected: Check\nSend Invite Link: {invite_link}",
    )
