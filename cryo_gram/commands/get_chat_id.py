from telegram import Update
from telegram.ext import ContextTypes

from cryo_gram.commands.utils import require_allowed_user


@require_allowed_user
async def get_chat_id(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text=f"chat_id: {update.effective_chat.id}")
