import functools
import logging
from typing import Any

from telegram import Update
from telegram.ext import ContextTypes

from cryo_gram.utils.cryo_admin import CryoAdminClient

LOGGER = logging.getLogger(__name__)
CRYO_ADMIN_CLIENT = CryoAdminClient()


def get_known_fridges(sending_username) -> dict[str, Any]:
    return CRYO_ADMIN_CLIENT.get_known_fridges(sending_username=sending_username)


def is_allowed_user(update: Update, context: ContextTypes.DEFAULT_TYPE) -> bool:
    username = update.effective_user.username
    if not CRYO_ADMIN_CLIENT.is_allowed_user(username):
        LOGGER.debug(f"Username {username} is not in the allowed users list")
        return False

    LOGGER.debug(f"Username {username} is allowed")
    return True


def get_chat_id(sending_username: str, fridge_name: str, org_name: str | None) -> tuple[int, str]:
    return CRYO_ADMIN_CLIENT.get_chat_id(sending_username, fridge_name, org_name)


def require_allowed_user(command_func):
    @functools.wraps(command_func)
    async def wrapper(update: Update, context: ContextTypes.DEFAULT_TYPE):
        if is_allowed_user(update, context):
            await command_func(update, context)
        else:
            await context.bot.send_message(
                chat_id=update.effective_chat.id,
                text="I'm sorry, but you seem to not have the neccessary permissions to allow me to interact with you",
            )

    return wrapper
