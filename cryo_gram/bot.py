import logging
import os

from telegram.ext import ApplicationBuilder, CommandHandler

from cryo_gram.commands.get_chat_id import get_chat_id
from cryo_gram.commands.get_fridge_list import get_fridge_list
from cryo_gram.commands.start import start
from cryo_gram.commands.subscribe import subscribe

LOGGER = logging.getLogger(__name__)


class CryoGram:
    def __init__(self) -> None:
        if "BOT_TOKEN" not in os.environ:
            raise Exception("No BOT_TOKEN found in the environment variables!")

        # Construct app
        self.app = ApplicationBuilder().token(os.environ.get("BOT_TOKEN")).build()

        # Add commands
        self.add_command("start", start)
        self.add_command("getchatid", get_chat_id)
        self.add_command("getfridgelist", get_fridge_list)
        self.add_command("subscribe", subscribe)

    def add_command(self, command_name, command_func):
        self.add_handler(CommandHandler(command_name, command_func))

    def add_handler(self, handler) -> None:
        self.app.add_handler(handler)

    def run_polling(self) -> None:
        self.app.run_polling()

    def run_webhook(self, *args, **kwargs) -> None:
        self.app.run_webhook(*args, **kwargs)
